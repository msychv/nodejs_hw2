module.exports = (Model) => async (req, res, next) => {
  const userId = req.userInfo._id;

  if (!await Model.exists({_id: userId})) {
    return res.status(400).json({message: 'Cannot find such user'});
  }

  next();
};
