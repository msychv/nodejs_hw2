module.exports = (Model) => async (req, res, next) => {
  const userId = req.userInfo._id;
  const noteId = req.params.id;

  if (!await Model.exists({userId, _id: noteId})) {
    return res.status(400).json({message: 'Cannot find such note'});
  }

  next();
};
