const express = require('express');
const router = new express.Router();
const {login, registration} = require('../controllers/authController');
const {asyncWrapper} = require('./middlewares/helpers');
const authValidation = require('./middlewares/authValidation');

router.post('/register',
    asyncWrapper(authValidation),
    asyncWrapper(registration));

router.post('/login',
    asyncWrapper(authValidation),
    asyncWrapper(login));

module.exports = router;
