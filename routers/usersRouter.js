const express = require('express');
const router = new express.Router();
const {
  getUser,
  deleteUser,
  updateUser,
} = require('../controllers/usersController');
const userModel = require('../models/userModel');
const doesUserExist = require('./middlewares/doesUserExist');
const {asyncWrapper} = require('./middlewares/helpers');
const jwtVerification = require('./middlewares/jwtVerification');
const passwordValidation = require('./middlewares/passwordValidation');

router.get('/',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(getUser));

router.delete('/',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(deleteUser));

router.patch('/',
    asyncWrapper(jwtVerification),
    asyncWrapper(passwordValidation),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(updateUser));

module.exports = router;
