const express = require('express');
const router = new express.Router();
const {
  createNote, getAllUserNotes, getNoteById,
  updateNote, checkUncheckNote, deleteNote,
} = require('../controllers/notesController');
const noteModel = require('../models/noteModel');
const userModel = require('../models/userModel');
const doesUserExist = require('./middlewares/doesUserExist');
const doesNoteExist = require('./middlewares/doesNoteExist');
const {asyncWrapper} = require('./middlewares/helpers');
const jwtVerification = require('./middlewares/jwtVerification');
const textValidation = require('./middlewares/textValidation');

router.post('/',
    asyncWrapper(textValidation),
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(createNote));

router.get('/',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(getAllUserNotes));

router.get('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesUserExist(userModel)),
    asyncWrapper(getNoteById));

router.put('/:id',
    asyncWrapper(textValidation),
    asyncWrapper(jwtVerification),
    asyncWrapper(doesNoteExist(noteModel)),
    asyncWrapper(updateNote));

router.patch('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesNoteExist(noteModel)),
    asyncWrapper(checkUncheckNote));

router.delete('/:id',
    asyncWrapper(jwtVerification),
    asyncWrapper(doesNoteExist(noteModel)),
    asyncWrapper(deleteNote));

module.exports = router;
