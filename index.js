require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');

const port = process.env.PORT || 8080;
const app = express();

app.use(morgan('tiny'));
app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://testuser:admin123@cluster0.mqjho.mongodb.net/notes?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
  } catch (error) {
    console.log(error.message);
  }

  app.listen(port, () => {
    console.log(`Server works at port ${port}!`);
  });
};

start();
