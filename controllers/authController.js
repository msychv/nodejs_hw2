const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET || 'secretword';

const User = require('../models/userModel');

const registration = async (req, res) => {
  const {username, password} = req.body;

  try {
    const user = new User({
      username,
      password: await bcrypt.hash(password, 10),
      createdDate: new Date(Date.now()),
    });

    await user.save();
    res.status(200).json({message: 'User created successfully!'});
  } catch (err) {
    if (err.code === 11000) {
      return res.status(400).json({
        message: `User with username \'${req.body.username}\' already exists`,
      });
    }
  }
};

const login = async (req, res) => {
  const {username, password} = req.body;
  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({
      message: `Cannot find user with ${username}!`,
    });
  }

  const isValid = await bcrypt.compare(password, user.password);

  if (!isValid) {
    return res.status(400).json({message: `Invalid password!`});
  }

  const {_id, createdDate} = user;
  const jwtToken = jwt.sign({
    _id,
    username,
    createdDate,
  }, JWT_SECRET);

  res.status(200).json({
    message: 'Success',
    jwt_token: jwtToken,
  });
};

module.exports = {registration, login};
