const Note = require('../models/noteModel');

const createNote = async (req, res) => {
  const text = req.body.text;
  const userId = req.userInfo._id;
  const note = new Note({
    userId,
    completed: false,
    text,
    createdDate: new Date(Date.now()),
  });

  await note.save();
  res.status(200).json({message: 'Note created successfully!'});
};

const getAllUserNotes = async (req, res) => {
  const userId = req.userInfo._id;
  const {limit = 6, offset = 0} = req.query;
  const userNotes = await Note.find({userId}, {__v: 0}, {
    limit: +limit > 100 ? 6 : +limit,
    skip: +offset,
    sort: {
      createdDate: -1,
    },
  });

  res.status(200).json({
    notes: userNotes,
    limit,
    offset: +limit + +offset,
  });
};

const getNoteById = async (req, res) => {
  const noteId = req.params.id;

  if (!await Note.exists({_id: noteId})) {
    return res.status(400).json({message: 'Cannot find such note'});
  }

  const note = await Note.findOne({_id: noteId}, {__v: false});
  res.status(200).json({note});
};

const updateNote = async (req, res) => {
  const noteId = req.params.id;
  const {text} = req.body;
  const note = await Note.findOne({_id: noteId});
  note.text = text;

  await note.save();
  res.status(200).json({message: 'Note updated successfully!'});
};

const checkUncheckNote = async (req, res) => {
  const noteId = req.params.id;

  await Note.updateOne({_id: noteId}, {completed: !this.completed});
  res.status(200).json({message: 'Note checked/unchecked successfully!'});
};

const deleteNote = async (req, res) => {
  const noteId = req.params.id;

  await Note.findByIdAndDelete({_id: noteId});
  res.status(200).json({message: 'Note deleted successfully!'});
};


module.exports = {
  createNote,
  getAllUserNotes,
  getNoteById,
  updateNote,
  checkUncheckNote,
  deleteNote,
};

