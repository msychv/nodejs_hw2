const mongoose = require('mongoose');
const noteSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    require: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
  },
});

module.exports = mongoose.model('Note', noteSchema);
